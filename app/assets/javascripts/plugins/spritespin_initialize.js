$(function(){
  $('.product3d').spritespin({
    // Set a single image url as source
    source: image3d,
    // Define the size of the display.
    // width: 480,
    // height: 480,
    width: inner_width,
    height: inner_height,
    // The total number of frames to show. Here it is 34 although
    // 36 images might fit into the sprite. But the last two are
    // missing as you can see in the image
    frames: 20,
    frameTime: 180,
    // The number of frames in one row of the sprite sheet
    // This can often be omitted. Spritespin might calculate the right number by its own.
    // framesX: 6,
    // Interaction sensitivity and direction
    // animate: false,
    sense: 1
  });
});


if(width > 640) {
  inner_width = 550
  inner_height = 550
}else{
  inner_width = 320
  inner_height = 320
}