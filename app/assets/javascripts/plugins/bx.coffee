$(document).ready ->
  
  $('.bxslider').bxSlider()

  about_us_slider = $('.about-us-slider').bxSlider
    controls: false
    pager: false
    speed: 1000
    pause: 10000
    auto: true
  $('.about-us-slider-wrap .prev-slide').click ->
    about_us_slider.goToPrevSlide()
  $('.about-us-slider-wrap .next-slide').click ->
    about_us_slider.goToNextSlide()


  banner_slider = $('.banner-slider').bxSlider
    controls: false
    pager: false
    speed: 1000
    pause: 5000
    mode: 'fade'
    # auto: true
    onSliderLoad: (currentIndex)->
    onSlideBefore: ($slideElement, oldIndex, newIndex)->
      $('.current-slide').text(banner_slider.getCurrentSlide()+1)
  if banner_slider.getSlideCount
    $('.total-slide').text(banner_slider.getSlideCount())
  $('.main-banner-wrapper .prev-slide').click ->
    current = banner_slider.getCurrentSlide()
    banner_slider.goToPrevSlide()
  $('.main-banner-wrapper .next-slide').click ->
    current = banner_slider.getCurrentSlide()
    banner_slider.goToNextSlide()