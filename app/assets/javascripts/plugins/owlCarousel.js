$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1200: {
        items: 4
      }
    }
  });
});


$(document).ready(function(){
  $qnty = $('.owl-dot').length
  $('.owl-dot').css("width","calc(100% / " + $qnty + ")")
});