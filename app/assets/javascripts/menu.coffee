$('document').ready ->

  $('.menu-button').on 'click', ->
    $wrap = $('body')
    $hamburger = $('.hamburger')
    if $wrap.hasClass('opened')
      $wrap.removeClass('opened')
      $hamburger.removeClass('is-active')
      $('.phone_drop_down').removeClass('active')
      $('.phone-button').removeClass('active')
      $('.catalog-wrap').removeClass('active')
      $('.drop_down_menu').removeClass('active')

      
    else
      $('body').addClass('opened')
      $hamburger.addClass('is-active')
      $('.catalog-wrap').removeClass('active')
      $('.drop_down_menu').removeClass('active')


  $('.drop-down-button').on 'click', ->
    if $('.catalog-wrap').hasClass('active')
      $('.catalog-wrap').removeClass('active')
      $('.drop_down_menu').hasClass('active')
      $('.drop_down_menu').removeClass('active')
      $('.phone_drop_down').removeClass('active')
      $('.phone-button').removeClass('active')
      $('body').removeClass('opened')
      $('.hamburger').removeClass('is-active')
      $('body').removeClass('menu-opened')

    else
      $('.catalog-wrap').addClass('active')
      $('body').addClass('menu-opened')
      $('.drop_down_menu').addClass('active')
      $('.phone_drop_down').removeClass('active')
      $('.phone-button').removeClass('active')
      $('body').removeClass('opened')
      $('.hamburger').removeClass('is-active')


  $('.phone-button').on "click", ->
    if $('.phone_drop_down').hasClass('active')
      $('.phone_drop_down').removeClass('active')
      $('.phone-button').hasClass('active')
      $('.phone-button').removeClass('active')
      $('.catalog-wrap').removeClass('active')
      $('.drop_down_menu').removeClass('active')
      $('body').removeClass('opened')
      $('.hamburger').removeClass('is-active')

    else
      $('.phone_drop_down').addClass('active')
      $('.phone-button').addClass('active')
      $('.catalog-wrap').removeClass('active')
      $('.drop_down_menu').removeClass('active')
      $('body').removeClass('opened')
      $('.hamburger').removeClass('is-active')



  $.clickOut('.phone_drop_down',
      ()->
        $('.phone_drop_down').removeClass('active')
        $('.phone').removeClass('active')
      {except: '.phone-button, .phone_drop_down'}
    )

  $.clickOut('.menu-button',
      ()->
        $('body').removeClass('opened')
        $('.hamburger').removeClass('is-active')
      {except: '.menu-button, baner_burger_menu'}
    )

  $.clickOut('.call-batton',
      ()->
        $('.popup_call').removeClass('active')

      {except: '.inner'}
    )


