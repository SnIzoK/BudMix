module ApplicationHelper
  def formatted_phone_number(phone)
    s = phone.gsub(/[\+\(\)]/) do |s|
      "<span>#{s}</span>"
    end

    s.html_safe
  end
end
