class PagesController < ApplicationController

  def index
    @google_map = true
    @slides = HomeSlide.published
    @home_page_info = HomePageInfo.first_or_initialize
    @features = AboutFeature.published.featured.includes(:translations)
    @testimonials = Testimonial.published
    @clients = Client.published
  end

  def about_us
    @about_page_info = AboutPageInfo.first_or_initialize
    @features = AboutFeature.published.includes(:translations)
    @certificates = Certificate.published.includes(:translations)
    @cases = ApplyCase.published
    @slides = AboutSlide.published
  end

  def catalog_page
  end

  def advice_page
    @advices = Advice.published.includes(:translations)
    @advices_tags = Cms::Tag.joins(:advices).where(advices: {published: 't'}).includes(:translations).uniq
  end

  def price_list_page
    @categories = CatalogCategory.published.joins(catalog_subcategories: :catalog_products).where(catalog_subcategories: {published: 't'}, catalog_products: {published: 't'}).includes(:translations, catalog_products: :translations).uniq
  end

  def contacts
    @google_map = true
    @office_categories = OfficeCategory.published.joins(:offices).where(offices: {published: 't'}, office_categories: {published: 't'}).includes(:translations, offices: [:translations, office_city: :translations]).uniq
    @office_cities = OfficeCity.joins(offices: :office_category).where(offices: {published: 't'}, office_categories: {published: 't'}).includes(:translations).uniq
  end

  def call
  end

  def manager_request
  end

end