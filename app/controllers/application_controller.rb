class ApplicationController < ActionController::Base

  include ActionView::Helpers::OutputSafetyHelper
  include Cms::Helpers::ImageHelper
  include ActionView::Helpers::AssetUrlHelper
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::UrlHelper
  include Cms::Helpers::UrlHelper
  include Cms::Helpers::PagesHelper
  include Cms::Helpers::MetaDataHelper
  include Cms::Helpers::NavigationHelper
  include Cms::Helpers::ActionView::UrlHelper
  include Cms::Helpers::Breadcrumbs
  include ActionControllerExtensions::InstanceMethods
  include ApplicationHelper
  include Cms::Helpers::AnotherFormsHelper
  include Cms::Helpers::TagsHelper
  include Cms::Helpers::AssetHelper
  reload_rails_admin_config
  initialize_locale_links
  def root_without_locale
    redirect_to root_path(locale: I18n.locale)
  end

  def render_not_found
    render template: "errors/not_found.html.slim", status: 404, layout: "application"
  end

  def catalog_categories
    @_catalog_categories ||= CatalogCategory.published.joins(catalog_subcategories: :catalog_products).where(catalog_subcategories: {published: 't'}, catalog_products: {published: 't'}).includes(:translations, catalog_subcategories: :translations).uniq
  end

  def subcategories_for(category)
    category.catalog_subcategories.published.joins(:catalog_products).where(catalog_products: { published: 't' }).uniq
  end

  helper_method :catalog_categories
  helper_method :subcategories_for
end
