class CatalogController < ApplicationController
  def index
    set_page_metadata(:catalog)
  end

  def category
    @category = CatalogCategory.get(params[:category])
    @subcategories = subcategories_for(@category)
    @products = @category.catalog_products.published.where(catalog_subcategories: {published: 't'}).includes(:translations)

    set_page_metadata(@category)
  end

  def subcategory
    @subcategory = CatalogSubcategory.get(params[:subcategory])
    if !@subcategory
      return render_not_found
    end
    @category = @subcategory.catalog_category
    @subcategories = subcategories_for(@category)
    @products = @subcategory.catalog_products.published.includes(:translations)

    set_page_metadata(@subcategory)

    render "category"
  end

  def product
    @product = CatalogProduct.get(params[:product])
    if !@product
      return render_not_found
    end

    @product_videos = @product.videos.published
    @related_products = @product.related_products
    @product_features = @product.features
    @product_image_url = @product.large_image.exists? ? @product.large_image.url : (@product.image.exists?(:medium) ? @product.image.url(:medium) : nil )

    @category = @product.catalog_category
    @subcategory = @product.catalog_subcategory

    add_breadcrumb(:home, root_path)
    add_breadcrumb(:catalog, catalog_path)
    add_breadcrumb(@category.name, @category.url)
    add_breadcrumb(@subcategory.name, @subcategory.url)

    set_page_metadata(@product)
  end
end