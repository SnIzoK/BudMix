class CatalogProduct < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :url_fragment, :short_description, :features, :usage_description, :preparing_basis, :preparing, :technical_parameters, :usage_guide

  belongs_to :catalog_subcategory
  attr_accessible :catalog_subcategory

  has_one :catalog_category, through: :catalog_subcategory
  attr_accessible :catalog_category

  has_many :videos, class_name: ProductVideo, foreign_key: :product_id
  accepts_nested_attributes_for :videos
  attr_accessible :videos, :videos_attributes

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_seo_tags
  has_sitemap_record
  has_cache do
    pages :all
  end

  image :image, styles: {medium: "420x420#", thumb: "55x45#"}
  image :large_image
  line_separated_field :features
  properties_fields :technical_parameters

  has_tags :pallets
  has_tags :packings
  has_tags :product_packaging_types


  define_resource_methods :get
  def url(locale = I18n.locale)
    base_url = catalog_subcategory.url(locale)
    if base_url.present?
      url_fragment = self.translations_by_locale[locale].try(&:url_fragment)
      if url_fragment.present?
        base_url += "/" if !base_url.end_with?("/")
        base_url + url_fragment
      else
        nil
      end
    else
      nil
    end
  end

  def related_products
    self.catalog_subcategory.catalog_products.published.where.not(catalog_products: {id: self.id}).limit(10)
  end

  def color
    catalog_category.try(:color)
  end


  def linkable_path
    "#{catalog_subcategory.linkable_path} -> #{name}"
  end
end