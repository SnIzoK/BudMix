class AboutFeature < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :title, :subtitle

  boolean_scope :published
  boolean_scope :featured
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :all
  end

end