class ProductVideo < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :url_fragment, :content, :vimeo_video_id, :youtube_video_id

  belongs_to :product, class_name: CatalogProduct, foreign_key: :product_id
  attr_accessible :product

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_seo_tags
  has_sitemap_record
  has_cache do
    pages CatalogProduct.all
  end

  image :image


  def video_url
    Cms::Helpers::VideoHelper.video_url(self)
  end

  def frame_video_url
    Cms::Helpers::VideoHelper.frame_video_url(self)
  end

  def video_provider
    Cms::Helpers::VideoHelper.video_provider(self)
  end
end