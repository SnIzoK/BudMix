class Client < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :website_url

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :all
  end

  image :image

  def image_extension
    image.exists? ? image_file_name.split(".").last : nil
  end
end