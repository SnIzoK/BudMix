class Office < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :address

  belongs_to :office_category
  belongs_to :office_city

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :all
  end

  line_separated_field :phones
  has_tags :office_tags

end