class AboutPageInfo < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :banner_label, :banner_title, :banner_subtitle, :intro, :slider_label, :apply_title, :certificates_title, :sales_representative_label, :sales_representative_title, :sales_representative_description, :regional_representative_label, :regional_representative_title, :regional_representative_description

  default_scope do
    order('id desc')
  end

  has_cache do
    pages :all
  end

  image :banner_image
end