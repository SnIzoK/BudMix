class HomeSlide < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :title, :subtitle, :short_description

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :all
  end

  has_link :linkable
  image :image
  image :background_image

  def slide_title
    s = self.title
    if s.present?
      return s
    else
      if linkable.is_a?(CatalogProduct)
        if (s = linkable.catalog_subcategory.try(:name)).present?
          return s
        else
          return nil
        end
      end
    end
  end

  def slide_subtitle
    s = self.subtitle
    if s.present?
      return s
    else
      if (s = linkable.try(:name)).present?
        return s
      else
        return nil
      end
    end
  end

  def slide_short_description
    s = self.short_description
    if s.present?
      return s
    else
      if (s = linkable.try(:short_description)).present?
        return s
      else
        return nil
      end
    end
  end

  def slide_image
    self.image.exists? ? self.image : self.linkable.try(:image)
  end


end