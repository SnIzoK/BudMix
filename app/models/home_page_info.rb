class HomePageInfo < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :catalog_title, :about_title, :about_content, :testimonials_title, :clients_title

  default_scope do
    order('id desc')
  end

  has_cache do
    pages :all
  end

end