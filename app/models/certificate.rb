class Certificate < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :short_description

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :all
  end

  image :image, styles: { small: "250x320#", popup: "1920x1080>" }
  pdf :document
end