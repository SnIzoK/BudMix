module RailsAdminDynamicConfig
  class << self
    def configure_rails_admin(initial = true)
      RailsAdmin.config do |config|

        ### Popular gems integration

        ## == Devise ==
        config.authenticate_with do
          warden.authenticate! scope: :user
        end
        config.current_user_method(&:current_user)

        ## == Cancan ==
        #config.authorize_with :cancan

        ## == Pundit ==
        # config.authorize_with :pundit

        ## == PaperTrail ==
        # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

        ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration



        if initial
          config.actions do
            dashboard                     # mandatory
            index                         # mandatory
            new do
              #except [CallRequest, ConsultationRequest, MeterRequest, ContactsRequest, PartnershipRequest]
            end
            export
            bulk_delete
            show
            edit
            delete
            show_in_app
            props do
              only []
            end
            #edit_model
            nestable do
              only [Cms::Page, OfficeCategory, Office, AboutFeature, AboutSlide, HomeSlide, Testimonial]
            end

            ## With an audit adapter, you can add:
            # history_index
            # history_show
          end


          config.parent_controller = "ApplicationController"
        end

        config.navigation_labels do
          [:feedbacks, :home, :catalog, :pricing, :about_us, :advices, :contacts, :tags, :users, :settings, :pages, :assets]
        end

        config.navigation_static_links = {
            mailchimp: "/admin/mailchimp",
            locales: "/file_editor/locales",
            site_data: "/file_editor/site_data.yml"
        }


        config.include_models Cms::SitemapElement, Cms::MetaTags
        config.include_models Cms::Page
        config.model Cms::Page do
          navigation_label_key(:pages, 1)
          nestable_list({position_field: :sorting_position, scope: :order_by_sorting_position})
          object_label_method do
            :custom_name
            #{
            #k = @bindings[:object].type.underscore.split("/").last
            #I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
            #}
          end
          list do
            sort_by do
              "sorting_position"
            end

            field :name do
              def value
                k = @bindings[:object].type.underscore.split("/").last
                I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
              end
            end

            field :h1_text do
              def value
                @bindings[:object].h1_text
              end
            end
          end

          edit do
            field :name do
              read_only true
              def value
                k = @bindings[:object].type.underscore.split("/").last
                I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
              end
            end
            
            field :translations, :globalize_tabs
            
            field :seo_tags

          end

        end
        
        config.model_translation Cms::Page do
          field :locale, :hidden
          field :h1_text
        end
        

        
        config.model Cms::MetaTags do
          visible false
          field :translations, :globalize_tabs
        end

        config.model_translation Cms::MetaTags do
          field :locale, :hidden
          field :title
          field :keywords
          field :description
        end
        

        config.model Cms::SitemapElement do
          visible false

          field :display_on_sitemap
          field :changefreq
          field :priority
        end

        config.include_models Attachable::Asset

        
        config.model Attachable::Asset do
          navigation_label_key(:assets, 1)
          field :data
          #watermark_position_field(:data)
          field :sorting_position
          field :translations, :globalize_tabs
        end

        config.model_translation Attachable::Asset do
          field :locale, :hidden
          field :data_alt
        end
        


        config.include_models User
        config.model User do
          navigation_label_key(:users, 1)
          field :email
          field :password
          field :password_confirmation
        end

        config.include_models Cms::Tag, Cms::Tagging

        config.model Cms::Tag do
          navigation_label_key(:tags, 1)
          
          field :translations, :globalize_tabs
          
          #field :videos
        end

        
        config.model_translation Cms::Tag do
          field :locale, :hidden
          field :name
          field :url_fragment do
            help do
              I18n.t("admin.help.#{name}")
            end
          end
        end
        

        config.model Cms::Tagging do
          visible false
        end

        # ===================================================
        # Requests
        # ===================================================
        config.configure_forms(:all)

        # ===================================================
        # Application specific models
        # ===================================================

        # Include models




        config.model CatalogCategory do
          navigation_label_key :catalog, 1
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :image
          field :color
          field :translations, :globalize_tabs
          field :seo_tags
        end
        
        config.model_translation CatalogCategory do
          field :locale, :hidden
          field :name
          field :url_fragment
        end
        config.model CatalogSubcategory do
          navigation_label_key :catalog, 2
          nestable_list({position_field: :sorting_position})
          object_label_method :formatted_name
        
          field :published
          field :catalog_category
          #field :image
          field :translations, :globalize_tabs
          field :seo_tags
        end
        
        config.model_translation CatalogSubcategory do
          field :locale, :hidden
          field :name
          field :url_fragment
        end
        config.model CatalogProduct do
          navigation_label_key :catalog, 3
          nestable_list({position_field: :sorting_position})

          list do
            field :published
            #field :catalog_category
            field :catalog_subcategory
            image_field :image
            translated_field(:name)
          end

          edit do
            field :published
            field :catalog_subcategory

            image_field :image
            image_field :large_image do
              help(help + " Показується на сторінці продукту. Якщо це поле пусте, то виводиться зображення з поля 'Картинка'")
            end
            field :product_packaging_types
            field :pallets
            field :packings

            field :translations, :globalize_tabs
            field :videos
            field :seo_tags
          end
        end
        
        config.model_translation CatalogProduct do
          field :locale, :hidden
          field :name
          field :url_fragment
          field :short_description
          field :features
          field :usage_description
          field :preparing_basis
          field :preparing
          field :technical_parameters
          field :usage_guide, :ck_editor
        end

        config.model ProductVideo do
          visible false

          field :published
          field :image
          field :translations, :globalize_tabs
        end

        config.model_translation ProductVideo do
          field :locale, :hidden
          field :name
          field :url_fragment
          field :content, :ck_editor
          field :youtube_video_id
          field :vimeo_video_id
          field :linkedin_presentation_url
        end

        config.model ProductPackagingType do
          visible false
          field :translations, :globalize_tabs
        end

        config.model_translation ProductPackagingType do
          field :name
        end


        config.model Advice do
          navigation_label_key :advices, 1
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :translations, :globalize_tabs
          field :tags
        end
        
        config.model_translation Advice do
          field :short_description
          field :content, :ck_editor
        end
        config.model OfficeCategory do
          navigation_label_key :contacts, 1
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :translations, :globalize_tabs
          #field :seo_tags
        end
        
        config.model_translation OfficeCategory do
          field :name
          field :url_fragment
        end
        config.model Office do
          navigation_label_key :contacts, 2
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :office_city
          field :office_category
          field :office_tags
          field :phones
          field :map_coordinates
          field :translations, :globalize_tabs
        end
        
        config.model_translation Office do
          field :name
          field :address
        end
        config.model OfficeCity do
          navigation_label_key :contacts, 3
        
          field :translations, :globalize_tabs
        end
        
        config.model_translation OfficeCity do
          field :name
          field :url_fragment
        end
        config.model AboutPageInfo do
          navigation_label_key :about_us, 1

          field :banner_image
          field :translations, :globalize_tabs
        end
        
        config.model_translation AboutPageInfo do
          field :banner_label
          field  :banner_title
          field :banner_subtitle
          field :intro, :ck_editor
          field :slider_label
          field :apply_title
          field :certificates_title
          field :sales_representative_label
          field :sales_representative_title
          field :sales_representative_description
          field :regional_representative_label
          field :regional_representative_title
          field :regional_representative_description
        end
        config.model AboutFeature do
          navigation_label_key :about_us, 2
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :featured
          field :translations, :globalize_tabs
        end

        config.model_translation AboutFeature do
          field :title
          field :subtitle
        end
        config.model Certificate do
          navigation_label_key :about_us, 5
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :image
          field :document
          field :translations, :globalize_tabs
        end
        
        config.model_translation Certificate do
          field :name
          field :short_description
        end
        config.model ApplyCase do
          navigation_label_key :about_us, 4
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :image
          linkable_field([CatalogCategory, CatalogSubcategory, CatalogProduct], :linkable, sort_by_path: true)
          field :translations, :globalize_tabs
        end
        
        config.model_translation ApplyCase do
          field :name
          field :short_description
        end
        config.model AboutSlide do
          navigation_label_key :about_us, 3
          nestable_list({position_field: :sorting_position})

          field :published
          field :image
          field :translations, :globalize_tabs
        end
        
        config.model_translation AboutSlide do
          field :name
        end
        config.model HomeSlide do
          navigation_label_key :home, 2
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :image
          field :background_image
          linkable_field([CatalogCategory, CatalogSubcategory, CatalogProduct], :linkable, sort_by_path: true)
          field :translations, :globalize_tabs
        end
        
        config.model_translation HomeSlide do
          field :title
          field :subtitle
          field :short_description
        end
        config.model HomePageInfo do
          navigation_label_key :home, 2

          field :translations, :globalize_tabs
        end
        
        config.model_translation HomePageInfo do
          field :catalog_title
          field :about_title
          field :about_content, :ck_editor
          field :testimonials_title
          field :clients_title
        end
        config.model Testimonial do
          navigation_label_key :home, 3
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :avatar
          field :translations, :globalize_tabs
        end
        
        config.model_translation Testimonial do
          field :name
          field :role
          field :short_description
        end
        config.model Client do
          navigation_label_key :home, 4
          nestable_list({position_field: :sorting_position})
        
          field :published

          field :image
          field :translations, :globalize_tabs
        end
        
        config.model_translation Client do
          field :name
          field :website_url
        end
        config.model ProductVideo do
          #navigation_label_key :about_us, 2
          nestable_list({position_field: :sorting_position})
        
          field :published
          field :image
          field :vimeo_video_id
          field :youtube_video_id
          field :translations, :globalize_tabs
          field :seo_tags
        end
        
        config.model_translation ProductVideo do
          field :name
          field :url_fragment
        end
      end
    end
  end
end