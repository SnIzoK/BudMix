class OfficeCity < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :url_fragment

  has_many :offices

  default_scope do
    order('id desc')
  end

  has_cache do
    pages :all
  end

end