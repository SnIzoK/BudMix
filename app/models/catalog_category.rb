class CatalogCategory < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :url_fragment

  has_many :catalog_subcategories
  has_many :catalog_products, through: :catalog_subcategories

  attr_accessible :catalog_subcategories, :catalog_subcategory_ids
  attr_accessible :catalog_products, :catalog_product_ids

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_seo_tags
  has_sitemap_record
  has_cache do
    pages :all
  end

  image :image, styles: {thumb: "80x80#", medium: "320x320#"}

  define_resource_methods :get, :url
  def self.base_url(locale = I18n.locale)
    Cms.url_helpers.send("catalog_#{locale}_path")
  end

  def linkable_path
    "Каталог -> #{name}"
  end
end