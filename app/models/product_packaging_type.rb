class ProductPackagingType < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name

  has_many :catalog_products

end
