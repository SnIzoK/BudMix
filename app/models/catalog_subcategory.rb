class CatalogSubcategory < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :url_fragment

  belongs_to :catalog_category
  has_many :catalog_products
  attr_accessible :catalog_products, :catalog_product_ids

  attr_accessible :catalog_category


  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_seo_tags
  has_sitemap_record
  has_cache do
    pages :all
  end

  image :image

  define_resource_methods :get
  def url(locale = I18n.locale)
    base_url = catalog_category.url
    if base_url.present?
      url_fragment = self.translations_by_locale[locale].try(&:url_fragment)
      if url_fragment.present?
        base_url += "/" if !base_url.end_with?("/")
        base_url + url_fragment
      else
        nil
      end
    else
      nil
    end
  end


  def linkable_path
    "#{catalog_category.linkable_path} -> #{name}"
  end

  def formatted_name
    category_name = catalog_category.try(:name)
    blank_name = "[без назви]"
    category_name = blank_name if category_name.blank?
    subcategory_name = self.name
    subcategory_name = blank_name if subcategory_name.blank?
    "#{category_name} -> #{subcategory_name}"
  end
end