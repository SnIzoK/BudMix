class Advice < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :short_description, :content

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :advices
  end

  has_tags :tags

end