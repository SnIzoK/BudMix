class OfficeCategory < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :url_fragment

  has_many :offices

  boolean_scope :published
  scope :order_by_sorting_position, -> { order('sorting_position asc') }
  default_scope do
    order_by_sorting_position
  end

  has_seo_tags
  has_sitemap_record
  has_cache do
    pages :all
  end


  define_resource_methods :get, :url, :year
  def self.base_url(locale = I18n.locale)
    Cms.url_helpers.send("blog_#{locale}_path")
  end
end