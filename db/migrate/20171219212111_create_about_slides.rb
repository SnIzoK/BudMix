class CreateAboutSlides < ActiveRecord::Migration
  def change
    create_table :about_slides do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.image :image
      t.timestamps null: false
    end
    create_translation_table(AboutSlide, :name)
  end
end