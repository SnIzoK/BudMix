class AddProductPackagingTypeIdToProducts < ActiveRecord::Migration
  def change
    add_column :catalog_products, :product_packaging_type_id, :integer
  end
end
