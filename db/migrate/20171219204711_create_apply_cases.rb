class CreateApplyCases < ActiveRecord::Migration
  def change
    create_table :apply_cases do |t|
      t.boolean :published
      t.integer :sorting_position
      t.image :image
      t.linkable :linkable
      t.string :name
      t.timestamps null: false
    end
    create_translation_table(ApplyCase, :name)
  end
end