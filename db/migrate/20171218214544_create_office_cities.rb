class CreateOfficeCities < ActiveRecord::Migration
  def change
    create_table :office_cities do |t|
      t.string :name
      t.string :url_fragment
      t.timestamps null: false
    end
    create_translation_table(OfficeCity, :name, :url_fragment)
  end
end