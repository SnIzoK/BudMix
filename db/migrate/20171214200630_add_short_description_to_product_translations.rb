class AddShortDescriptionToProductTranslations < ActiveRecord::Migration
  def change
    add_column :catalog_product_translations, :short_description, :text
  end
end
