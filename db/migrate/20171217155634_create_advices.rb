class CreateAdvices < ActiveRecord::Migration
  def change
    create_table :advices do |t|
      t.boolean :published
      t.integer :sorting_position
      t.text :short_description
      t.text :content
      t.timestamps null: false
    end
    create_translation_table(Advice, :short_description, :content)
  end
end