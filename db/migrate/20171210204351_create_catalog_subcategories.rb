class CreateCatalogSubcategories < ActiveRecord::Migration
  def change
    create_table :catalog_subcategories do |t|
      t.boolean :published
      t.integer :sorting_position
      t.integer :catalog_category_id
      t.string :name
      t.string :url_fragment
      t.image :image
      t.timestamps null: false
    end
    create_translation_table(CatalogSubcategory, :name, :url_fragment)
  end
end