class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.boolean :published
      t.integer :sorting_position
      t.image :avatar
      t.string :name
      t.string :role
      t.text :short_description
      t.timestamps null: false
    end
    create_translation_table(Testimonial, :name, :role, :short_description)
  end
end