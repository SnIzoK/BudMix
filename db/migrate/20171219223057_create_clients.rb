class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.string :website_url
      t.image :image
      t.timestamps null: false
    end
    create_translation_table(Client, :name, :website_url)
  end
end