class CreateOfficeCategories < ActiveRecord::Migration
  def change
    create_table :office_categories do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.string :url_fragment
      t.timestamps null: false
    end
    create_translation_table(OfficeCategory, :name, :url_fragment)
  end
end