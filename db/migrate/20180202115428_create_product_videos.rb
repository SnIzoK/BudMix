class CreateProductVideos < ActiveRecord::Migration
  def change
    create_table :product_videos do |t|
      t.integer :product_id
      t.boolean :published
      t.string :name
      t.integer :sorting_position
      t.string :url_fragment
      t.text :content
      t.image :image
      t.string :vimeo_video_id
      t.string :youtube_video_id
      t.timestamps null: false
    end
    add_index :product_videos, :product_id
    create_translation_table(ProductVideo, :name, :url_fragment, :content, :vimeo_video_id, :youtube_video_id)
  end
end