class AddFeaturesToCatalogProducts < ActiveRecord::Migration
  def change
    add_column :catalog_products, :features, :text
    add_column :catalog_product_translations, :features, :text
  end
end
