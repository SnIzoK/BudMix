class AddShortDescriptionToUseCases < ActiveRecord::Migration
  def change
    add_column :apply_cases, :short_description, :text
    add_column :apply_case_translations, :short_description, :text
  end
end
