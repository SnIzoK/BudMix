class AddMapCoordinatesToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :map_coordinates, :string
  end
end
