class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.text :address
      t.text :phones
      t.timestamps null: false
    end
    create_translation_table(Office, :name, :address)
  end
end