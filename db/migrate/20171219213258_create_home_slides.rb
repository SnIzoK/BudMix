class CreateHomeSlides < ActiveRecord::Migration
  def change
    create_table :home_slides do |t|
      t.boolean :published
      t.integer :sorting_position
      t.image :image
      t.image :background_image
      t.string :title
      t.string :subtitle
      t.text :short_description
      t.linkable :linkable
      t.timestamps null: false
    end
    create_translation_table(HomeSlide, :title, :subtitle, :short_description)
  end
end