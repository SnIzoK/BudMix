class AddOfficeCategoryIdToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :office_category_id, :integer
  end
end
