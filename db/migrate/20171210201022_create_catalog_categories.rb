class CreateCatalogCategories < ActiveRecord::Migration
  def change
    create_table :catalog_categories do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.string :url_fragment
      t.image :image
      t.string :color
      t.timestamps null: false
    end
    create_translation_table(CatalogCategory, :name, :url_fragment)
  end
end