class AddFeaturedToAboutFeatures < ActiveRecord::Migration
  def change
    add_column :about_features, :featured, :boolean
  end
end
