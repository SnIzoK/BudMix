class CreateHomePageInfos < ActiveRecord::Migration
  def change
    create_table :home_page_infos do |t|
      t.string :catalog_title
      t.string :about_title
      t.text :about_content
      t.string :testimonials_title
      t.string :clients_title
      t.timestamps null: false
    end
    create_translation_table(HomePageInfo, :catalog_title, :about_title, :about_content, :testimonials_title, :clients_title)
  end
end