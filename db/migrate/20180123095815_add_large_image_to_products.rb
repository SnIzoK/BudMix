class AddLargeImageToProducts < ActiveRecord::Migration
  def change
    change_table :catalog_products do |t|
      t.attachment :large_image
    end
  end
end
