class AddOfficeCityIdToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :office_city_id, :integer
  end
end
