class CreateProductPackagingTypes < ActiveRecord::Migration
  def change
    create_table :product_packaging_types do |t|
      t.string :name

      t.timestamps null: false
    end

    create_translation_table(ProductPackagingType, :name)
  end
end
