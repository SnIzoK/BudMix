class CreateAboutFeatures < ActiveRecord::Migration
  def change
    create_table :about_features do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :title
      t.string :subtitle
      t.timestamps null: false
    end
    create_translation_table(AboutFeature, :title, :subtitle)
  end
end