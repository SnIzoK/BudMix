class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.text :short_description
      t.image :image
      t.attachment :document
      t.timestamps null: false
    end
    create_translation_table(Certificate, :name, :short_description)
  end
end