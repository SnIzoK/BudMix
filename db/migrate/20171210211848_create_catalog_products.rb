class CreateCatalogProducts < ActiveRecord::Migration
  def change
    create_table :catalog_products do |t|
      t.boolean :published
      t.integer :sorting_position
      t.integer :catalog_subcategory_id
      t.string :name
      t.string :url_fragment
      t.text :short_description
      t.text :usage_description
      t.text :preparing_basis
      t.text :preparing
      t.text :technical_parameters
      t.text :usage_guide
      t.image :image
      t.timestamps null: false
    end
    create_translation_table(CatalogProduct, :name, :url_fragment, :usage_description, :preparing_basis, :preparing, :technical_parameters, :usage_guide)
  end
end