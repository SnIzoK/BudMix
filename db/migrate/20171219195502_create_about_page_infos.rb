class CreateAboutPageInfos < ActiveRecord::Migration
  def change
    create_table :about_page_infos do |t|
      t.string :banner_label
      t.string :banner_title
      t.string :banner_subtitle
      t.image :banner_image
      t.text :intro
      t.string :slider_label
      t.string :apply_title
      t.string :certificates_title
      t.string :sales_representative_label
      t.string :sales_representative_title
      t.text :sales_representative_description
      t.string :regional_representative_label
      t.string :regional_representative_title
      t.text :regional_representative_description
      t.timestamps null: false
    end
    create_translation_table(AboutPageInfo, :banner_label, :banner_title, :banner_subtitle, :intro, :slider_label, :apply_title, :certificates_title, :sales_representative_label, :sales_representative_title, :sales_representative_description, :regional_representative_label, :regional_representative_title, :regional_representative_description)
  end
end