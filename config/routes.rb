Rails.application.routes.draw do
  mount Cms::Engine => '/', as: 'cms'
  mount Ckeditor::Engine => '/ckeditor'
  get "admin(/*admin_path)", to: redirect{|params| "/#{ I18n.default_locale}/admin/#{params[:admin_path]}"}
  root as: "root_without_locale", to: "application#root_without_locale"
  localized do
    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
    devise_for :users, module: 'users', path: :users, path_names: {
      sign_in: 'login',
      sign_out: 'logout',
    }

    root to: "pages#index"

    scope "catalog", controller: "catalog" do
      root action: :index, as: :catalog
      get ":category", action: :category, as: :catalog_category
      get ":category/:subcategory", action: :subcategory, as: :catalog_subcategory
      # get ":category/:subcategory/:product", action: :product, as: :catalog_product
      get "/:category/:subcategory/:product", action: :product, as: :catalog_product
    end

    controller :pages do
      get "about_us", action: "about_us"
      # get "contact-us", action: "contact_us"
      get "advice_page",  action: "advice_page"
      get "price_list_page", action: "price_list_page"
      get "contacts", action: "contacts"
      post "call", action: "call"
      post "manager_request", action: "manager_request"
    end

  end

  match '*url', to: 'application#render_not_found', via: :all
end